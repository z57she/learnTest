
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Arrays;

/**
 * 数组移除一个元素，得到新数组即一种组合形式。
 * 新数组重复上面移除操作，直到长度=2。
 *
 */
public class Demo {

    private static Map<String, String[]> mp = new HashMap<>();

    public static void main(String[] args) {
        String[] arr = { "a", "b", "c", "d" };
        newArray(arr);
        System.out.println(mp.size());
        for (String[] item : mp.values()) {
            System.out.println(Arrays.toString(item));
        }
    }

    public static void newArray(String[] arr) {
        if (arr.length > 2) {
            List<String> list;

            for (int i = 0; i < arr.length; i++) {

                //String[] => List
                // for (String s : arr) {
                //     list.add(s);
                // }
                list = new ArrayList<String>(Arrays.asList(arr));
                list.remove(i);

                //List => String[]
                // String[] str = new String[list.size()];

                // for(int j = 0; j < list.size(); j++) {
                //     str[j] = list.get(j);
                // }
                String[] str = list.toArray(new String[0]);

                //去重
                if (!mp.containsKey(list.toString())) {
                    // System.out.println(list.toString() + " " + Arrays.toString(str));
                    mp.put(list.toString(), str);
                }

                list.clear();
                newArray(str);
            }

        }
    }

}
